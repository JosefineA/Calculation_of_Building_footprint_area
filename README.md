# FME-skript för automatiskt beräkning av byggnadsarea
Skripten *Automatisk beräkning - IFC-modell utan objekt för BYA*, *Automatisk beräkning - IFC-modell med objekt för BYA* 
och *Automatisk beräkning med IfcSpace-objekt* är släppta under Open Source licensen Berkley Software Distribution BSD (https://opensource.org/licenses/BSD-3-Clause). 
De BIM-modeller som testats finns dock inte tillgängliga då de inte innefattas av BSD-licensen.

## Översikt över skript
Skripten läser in en IFC-fil och beräknar utifrån olika IFC-element byggnadsarean (BYA). 
Vilka IFC-element som krävs beror på modellens Level of Development (LoD), konstruktion och komplexitet. 
Vanligast förekommande är: *IfcBuildingStorey*, *IfcSlab*, *IfcWall*, *IfcWallStandardCase* och *IfcRoof*.
* Workspace - utan objekt för BYA (*Automatisk beräkning - IFC-modell utan objekt för BYA*)
	* Hjältevadshus Spira 168 - utan objekt för BYA
	* Hjältevadshus Spira 175 - utan objekt för BYA
	* Kamakura - utan objekt för BYA
	* KTHdemohus - utan objekt för BYA
	* Multihuset - utan objekt för BYA
	* Nyvångskolan_Byggnad_F - utan objekt för BYA
	* Nyvångskolan_Byggnad_H - utan objekt för BYA
	* Revit - utan objekt för BYA
	
I denna beräkning krävs att arkitekt/konstruktör som skapat modellen har ritat in en yta eller volym som representerar BYA.
Från IFC-element *IfcSpace* väljs det objekt som representerar BYA ut och arean utifrån detta objekt beräknas.
* Workspace - med objekt för BYA (*Automatisk beräkning - IFC-modell med objekt för BYA*)
	* Kamakura - med objekt för BYA
	* Nyvångskolan_Byggnad_F - med objekt för BYA
	* Nyvångskolan_Byggnad_H - med objekt för BYA
	
Detta skript är en kombination av ovanstående. För en modell finns ett *IfcSpace*-objekt som representerar bruttoarea (BTA) som kan användas för beräkning då ovanstående skript kombineras.	
* Workspace - med IfcSpace-objekt (*Automatisk beräkning med IfcSpace*-objekt)
	* KTHdemohus - IfcSpace objekt
	
	
> Skripten är designade och utvecklade för examensarbetet *Automatisering av bygglovsansökningsprocessen med stöd av BIM och GIS*.
> Lunds Tekniska Högskola, Institutionen för Naturgeografi och Ekosystemvetenskap.